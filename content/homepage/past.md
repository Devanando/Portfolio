---
title: "Past Projects"
weight: 4
header_menu: true
---

From here on you will find some of the projects I have done. They are ordered by date of completion of the project

---

##### Booking system Agapi B.V.

To make my current work at Agapi b.v. easier, I have decided to create a new booking program with the help of another Codam student.
The old one was written with visual basic while using microsoft access as a database. However, since this is a bit outdated I thought it was time for something new. It is being made with C# in the .net CORE 3 framework and will use either a local database (sqlite) or a database running on my raspberry pi 4 (mysql).

[link to repository](https://gitlab.com/Devanando/agapioffice_program)

while i was working on this system I started working at myTomorrows as a backend engineer and left my position at Agapi. I therefore abandoned the projects as it was not relevant for me anymore. 

##### 42sh - Our own shell

This was the first major project which I had to to do with four teammembers. We had to create out own shell with several functions included.  
Some of these functions are shown below:  
- Lexer/tokenizer and parsing the input.
- Complete input handling (cut, copy and paste).
- Contextual completion.
- History managment.
- Job control.
- Hash table.
- Complete pipe, heredoc and redirection handling.
- Our own error messages without the use of errno.
- Look in the repository to find out which extra functionalities are included.
  
![Picture of cetushell in action](images/cetushell.jpg)

[42shell repository.](http://gitlab.com/devanando/42shell)


21sh and minishell were the prerequisites of 42shell and learned us step by step what the best method was to build up a shell.
these projects can be found in the following repositories. All these projects were written in C.

[21shell repository.](http://gitlab.com/devanando/21shell)  
[minishell repository.](http://gitlab.com/devanando/21shell)

Many thanks to [Liewe Gutter](http://liewegutter.com), [Noah Loomans](http://noahloomans.com) and Arend Holster for being a great team to work with.

---

##### ft_ls - Recreation of linux list function

In this project I had to learn about the basics of file modes and the read, write or execution rights. ft_ls is a recreation of the linux list command and had to work with atleast the following flags -l -R -a -r -t.
We have added some bonus flags for extra points in the project.
The bonus flags were -u -n -T -S -o. This project is written in C.


[ft_ls repository.](https://gitlab.com/nloomans/ft_ls)

---

##### ft_contrast - Change the contrast of an image with the option of utilizing multi-threading

This project was a 'Rush' meaning it has to be completed and turned in after 48 hours. In this project a .pgm image file was given with the
intention of changing the contrast of the image. After the change in contrast functioned, we had to be able to make this multi-threaded to speed up the program
For bonus we have added a change in brightness for the given image. This project is written in C.

[ft_contrast repository.](https://gitlab.com/devanando/ft_contrast)

---

##### Home automation - No physical light switches

When my father moved to his new house, I had the idea to set up a system were he could use his phone or a tablet to control all the lights in his house. 
I did this with the help of [Home Assistent](https://www.home-assistant.io/hassio/) and wifi modules esp8266 (Sonoff). I had to flash the Sonoff with 
opensource firmware made by others ([Tasmota firmware](https://github.com/arendst/Tasmota)).  
    
Hassio itself uses .yaml files and I used a mqtt broker for the devices
to connect to each other through a local wifi network. If the system detects any issues it can restart on its own. It is only able to change these settings or the state of the lights 
through the local network and is not exposed to the outer world for security reasons. The system is still running and is working succesfully.


---

##### ft_turing - A Python rush project

For practicing a technical assessment, Codam invited several tomtom software engineers to look at a project and receive feedback.
The project consisted of making a turing machine which took a json as input. It had to be done in Python, this was an other 'Rush' project (48 hours to complete and turn in the project)"

[ft_turing repository.](https://gitlab.com/devanando/ft_turing)


---

##### PHP piscine - Training for PHP, SQL, HTML, CSS and JS

In the PHP piscine you learn about web development and all necessary modules that come with it. Everyday new assignments were given which would each introduce a different problem, all assignments are written in PHP/SQL, html and css. 
One of the 'Rush' that was given within this period was to create your own webshop using PHP and SQL.

[php_piscine repository.](https://gitlab.com/devanando/piscine-php)