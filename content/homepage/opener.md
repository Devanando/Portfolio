---
title: "Welcome"
weight: 1
---

Hello there and welcome to my portfolio page! 
This page contains a bit of information about me and my career path. 
You can find my contact details if you are interested or have any questions.