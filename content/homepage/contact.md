---
title: "Contact"
weight: 5
header_menu: true
---

E-Mail: [D.kroeke@gmail.com](mailto:D.kroeke@gmail.com)

Tel.: [+316 4395 1460](tel:+31643951460)

[https://linkedin.com/in/dkroeke/](https://www.linkedin.com/in/dkroeke/)

Interested or have any questions? Feel free to contact me and let us get in touch!