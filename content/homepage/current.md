---
title: "Career Path"
weight: 3
header_menu: true
---

Hands-on programmer with 8+ years experience in business management across various highly regulated industries. Strong suit in embedded backend software, Databases, DevOps and mobile app development. Experience with various AWS services, including Cloudformation for CI/CD. Thriving on coding in C, Python and Dart with additional background in C#, PHP, SQL, JS & HTML. Ambitious learner and teamplayer who is always up for new challenges.  

##### Senior Backend Engineer
###### myTomorrows
###### Jun 2023 – Present

Continued journey as senior lead in the Backend Engineering team. 

• Collaboration with product management to develop myTomorrows' client solution portfolio  
• Key SME within myTomorrows core client projects in the hospital and pharma industry  
• Technical advisor for the core CRM team: Lead of Data Migration (CRM Cloud) and ETL pipeline setting for data warehouses  

##### Backend Engineer
###### myTomorrows
###### May 2021 – Jun 2023

Continued full-time employment with focus on Backend Engineering and in support of the DevOps Engineering team. 

• Coaching of junior software engineers in the team  

##### Intern Backend & DevOps Engineer
###### myTomorrows
###### Oct 2020 – May 2021 (8 mos)

Internship as DevOps & Backend Engineer with engagement in different parts of the MyTomorrows product foundation. 

Gaining hands-on experience in:  
• AWS Services: Cloudformation, ElasticBeanstalk, EC2, Cloudfront, APIGateway, RDS, Route53 and Lambda  
• Leading a cloud spend optimization initiative and achieving a cost reduction by 75%  
• Creation of internal and external facing API communication layers (Python) by utilizing Microservice architecture  
• Application of Continuous Implementation and -Deployment Principles (CI/CD)  
• Introducing a quality assurance process for workflow creation to support CI across product branches via Github Actions  


##### Director of Business Operations
###### Agapi bv
###### Oct 2015 – May 2021 (5 yrs 8 mos)

Director of day-to-day business operations as part of the family-managed window-rental company in the Amsterdam Redlight District. 

Responsibilities:  
• Company representation in front of various business stakeholders, e.g. the Amsterdam city council, public health authorities (GGD) and property owners  
• Assurance of Compliance with applicable laws by international clients as well as employees at all times  
• Team Lead of a headcount of 11 FTE  
• Operational Management of client reservations, facility- and security services and payments  
• Revenue- and Tax Reporting  
• Helping hand to "Get Things Done" where needed  
• Development of a technical solution for company record keeping  


##### Bartender
###### Caharo B.V.
###### Oct 2011 – Nov 2015 (4 yrs 2 mos)

Bartender in one of the most vibrant bars in the Redlight District of Amsterdam, also known as the "Bananabar". All-round team member behind the bar during and outside of erotic presentations and happy-hours. Direct exposure to customers from around the world.

[link to my Curriculum Vitea](https://gitlab.com/Devanando/cv/-/blob/df5b6c41036199a9cfdc2752408d2c532e58beb4/C.V.%20Devanando%20Kroeke.pdf)  

If you got interested, please don't hesitate to contact me (You'll find my contact details below).



---
