---
title: "About Me"
weight: 2
header_menu: true
---

I have explored many different paths, but programming is the one I keep returning to. 13 years ago I first came in contact with the coding world researching ways to create hacks for games which I created with visual basics at that time.
Possibilities seemed endless with coding, changing the behaviour of how a game was initially designed sparked my interests. I started setting up my own private servers of certain games and tried to tweak or change models and items in the game. After some life changing events my interests in coding got lost and I took another road.
  
![Devanando Kroeke](images/Pasfoto.jpeg)
  
I went on to study Aviation engineering for five years. Upon nearing completion me and my father started a company called Agapi B.V., which is a window rental company located in Amsterdam. Now again five years later, the work at Agapi B.V. was not challenging enough for me and I felt there was no personal progress for a while. I knew I had to search something that was going to challenge me and thus keep me motivated. I started searching and Codam Coding College crossed my path. I have reinvented my interest in coding and now I am sure I will stay in the coding world.

job experiences:
- Teamleader at Albert heijn
- Bartender at Bananabar
- Internship:  Assistent maintenance manager at Nayak
- Manager at Agapi b.v.
- Senior Software Engineer (current)